from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.principal),
    path('logout', views.logout_view),
    path('errase', views.errase),
    path('ayuda', views.ayuda),
    path('register', views.register_view),
    path('usuarios/<str:user>', views.mipagina),
    path('<str:palabra>', views.buscapalabra)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
