from django.test import TestCase
from django.test import Client
from .models import User, Palabras, Enlaces, Comentarios, Votos
import datetime

# Se comprueban los GET y POST para cada recurso. Como adicional en la mitad se comprueba cosas como redirección de un
# enlace, iniciar sesion sin existir cuenta, elementos query o que no aparezca el elemento cuando me encuentro en la
# página correspondiente


class ayudaTest(TestCase):

    def test_GETayuda(self):
        c = Client()
        response = c.get('/mispalabras/ayuda')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3> Funcionamiento de MisPalabras </h3>', content)
        # Comprobación cosas extra, por ejemplo, que no esté el botón
        self.assertNotIn('Ayuda', content)


class inicioTests(TestCase):

    def test_GETinicio(self):
        c = Client()
        response = c.get('/mispalabras')
        self.assertEqual(response.status_code, 301)
        content = response.content.decode('utf-8')
        # Comprobación cosas extra, por ejemplo, que no esté el botón
        self.assertNotIn('Inicio', content)

    def test_POSTpalabra(self):
        c = Client()
        response = c.post('/mispalabras/', {'palabra': "sol", 'name': "formpalabra"})
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('sol', content)


class loginTest(TestCase):

    def test_POSTlogin(self):
        c = Client()
        response = c.post('/mispalabras/', {'username': "admin", 'password': "admin", 'name': "login"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<ul class="pagination">', content)


class registerTests(TestCase):

    def test_GETregister(self):
        c = Client()
        response = c.get('/mispalabras/register')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Registrese', content)

    def test_POSTregister(self):
        c = Client()
        response = c.post('/mispalabras/register',
                          {'username': "bibi", 'password': "bibi", 'email': "bibi%40gmail.com", 'name': "register"})
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('/mispalabras', content)


class erraseTest(TestCase):

    def test_GETerrase(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('/mispalabras/errase')
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('/mispalabras', content)


class logoutTest(TestCase):

    def test_GETlogout(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get('/mispalabras/logout')
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('/mispalabras/', content)


class paginaTest(TestCase):

    def test_GETmipagina(self):
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.post('/mispalabras/sol', {'name': "save"})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/mispalabras/usuarios')
        content = response.content.decode('utf-8')
        self.assertIn('sol',content)


# Suponemos que no hay sesion iniciada
class palabranoexisteTests(TestCase):

    def setUp(self):
        c = Client()
        c.post('/mispalabras/register', {'username': "admin", 'password': "admin", 'email': "admin%40admin.es",
                                         'name': "register"})

    def test_GETpalabra(self):
        c = Client()
        response = c.get('/mispalabras/sol')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Helios', content)

    def test_POSTpalabra(self):
        c = Client()
        response = c.post('/mispalabras/', {'palabra': "luz", 'name': "formpalabra"})
        self.assertEqual(response.status_code, 302)
        content = response.headers['Location']
        self.assertIn('luz', content)

        response = c.get('/mispalabras/luz')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('luz', content)


# Guardamos la palabra para que ya exista
class palabraexisteTests(TestCase):

    def setUp(self):
        user = User.objects.create_user("testuser", "testuser@testuser.es", "testuser")
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        c = Palabras(valor="luna", definicion="Nuestra luna", imagen_wiki="www.luna.es", usuario=user, fecha=datetime.datetime.now())
        c.save()

    def test_GETpalabra(self):
        c = Client()
        response = c.get('/mispalabras/luna')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('comentario', content)

    def test_POSTlike(self):
        response = self.client.post('/mispalabras/luna', {'name': "like"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Delete Vote', content)
        # Comprobación cosas extra, por ejemplo, que esta en la base de datos
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(valor="luna")
        voto = Votos.objects.get(palabra=c, usuario=user)
        self.assertEqual(1, voto.valor)

    def test_POSTdislike(self):
        response = self.client.post('/mispalabras/luna', {'name': "dislike"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Like', content)
        # Comprobación cosas extra, por ejemplo, que esta en la base de datos
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(valor="luna")
        voto = Votos.objects.get(palabra=c, usuario=user)
        self.assertEqual(-1, voto.valor)

    def test_POSTRAE(self):
        response = self.client.post('/mispalabras/luna', {'name': "RAE"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('El contenido de la RAE es:', content)

    def test_POSTflickr(self):
        response = self.client.post('/mispalabras/luna', {'name': "flickr"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('flickr', content)

    def test_POSTenlace(self):
        response = self.client.post('/mispalabras/luna', {'name': "enlace", 'valor': "https://www.elmundo.es"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('https://www.elmundo.es', content)
        # Comprobación cosas extra, por ejemplo, que esta en la base de datos
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(valor="luna")
        enlace = Enlaces.objects.get(palabra=c, usuario=user)
        self.assertEqual("https://www.elmundo.es", enlace.valor)

    def test_POSTcomentario(self):
        response = self.client.post('/mispalabras/luna', {'name': "comentario", 'valor': "Comentario de prueba"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Comentario de prueba', content)
        # Comprobación cosas extra, por ejemplo, que esta en la base de datos
        user = User.objects.get(username="testuser")
        c = Palabras.objects.get(valor="luna")
        comentario = Comentarios.objects.get(palabra=c, usuario=user)
        self.assertEqual("Comentario de prueba", comentario.valor)

    def test_POSTmeme(self):
        response = self.client.post('/mispalabras/luna', {'name': "meme", 'texto': "Meme prueba", 'opcionmeme': 1})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('El meme actual es:', content)