import datetime
import urllib
from itertools import chain
import random
from urllib.request import urlopen
import ssl
import json
import cloudscraper
from bs4 import BeautifulSoup
from django.contrib.auth.models import User
from django.core import serializers
from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import logout, login, authenticate
from .models import Palabras, Votos, Comentarios, Enlaces, Imagenes
from xml.etree.ElementTree import parse

# Constantes
MOSTRAR_N_MAS_VOTADO = 10
INICIO_RAE = "1."
N_POR_PAGINA = 5


def get_lista_palabras(pag):
    lista_final = []
    palabrasbase = Palabras.objects.all().values()
    lista = (sorted(palabrasbase, key=lambda x: x['fecha'], reverse=True))
    for palabra in lista:
        try:
            pal = Palabras.objects.get(valor=palabra['valor'])
            votosbase = Votos.objects.filter(palabra=pal).values()
        except:
            pass
        nvotos = 0
        for voto in votosbase:
            nvotos = nvotos + voto['valor']
        dict = {}
        dict['palabra'] = pal.valor
        dict['votos'] = nvotos
        dict['definicion'] = pal.definicion
        dict['imagen'] = pal.imagen_wiki
        dict['usuario'] = pal.usuario.username
        lista_final.append(dict)
    return lista_final[N_POR_PAGINA*(int(pag)-1):(N_POR_PAGINA+N_POR_PAGINA*(int(pag)-1))]


def get_scroll_nums(pag):
    npags = get_num_list()
    if pag:
        if npags == 1:
            prevpag = 1
            nextpag = 1
        else:
            if pag == 1:
                prevpag = pag
                nextpag = str(int(pag) + 1)
            elif pag == npags:
                prevpag = str(int(pag) - 1)
                nextpag = pag
            else:
                prevpag = str(int(pag) - 1)
                nextpag = str(int(pag) + 1)
    else:
        prevpag = str(1)
        nextpag = str(1)
    return prevpag, list(range(1, get_num_list() + 1)), nextpag


def process_file(asked_file, palabra):
    if asked_file:
        if asked_file == "xmlpals":
            data = serializers.serialize("xml", Palabras.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif asked_file == "jsonpals":
            data = serializers.serialize("json", Palabras.objects.all())
            return HttpResponse(data, content_type='text/json')
        elif asked_file == "xmlcomen":
            data = serializers.serialize("xml", Comentarios.objects.all())
            return HttpResponse(data, content_type='text/xml')
        elif asked_file == "jsoncomen":
            data = serializers.serialize("json", Comentarios.objects.all())
            return HttpResponse(data, content_type='text/json')
        elif asked_file == "xmlpal":
            if palabra:
                palabrabase = Palabras.objects.get(valor=palabra)
                comentariosbase = Comentarios.objects.filter(palabra=palabrabase)
                enlacesbase = Enlaces.objects.filter(palabra=palabrabase)
                listatotal = list(chain(comentariosbase, enlacesbase))
                listatotal.append(palabrabase)
                data = serializers.serialize("xml", listatotal)
                return HttpResponse(data, content_type='text/xml')
        elif asked_file == "jsonpal":
            if palabra:
                palabrabase = Palabras.objects.get(valor=palabra)
                comentariosbase = Comentarios.objects.filter(palabra=palabrabase)
                enlacesbase = Enlaces.objects.filter(palabra=palabrabase)
                listatotal = list(chain(comentariosbase, enlacesbase))
                listatotal.append(palabrabase)
                data = serializers.serialize("json", listatotal)
                return HttpResponse(data, content_type='text/json')
    else:
        pass


def get_example_word():
    base = Palabras.objects.all().values_list('valor')
    try:
        palabra = str(random.choice(base)).split("'")[1]
    except:
        palabra = ""
    return palabra


def manage_comun_post(request):
    template = ""
    context = {}
    red = ""
    if request.method == "POST":
        if request.POST['name'] == "formpalabra":
            valor = "/mispalabras/" + request.POST['palabra']
            red = redirect(valor)
        elif request.POST['name'] == "login":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
    return template, get_main_elements(request), red


def register_view(request):
    context = {}
    if request.method == "GET":
        template = loader.get_template('register.html')
    elif request.method == "POST":
        if request.POST['name'] == "register":
            username = request.POST['username']
            try:
                user = User.objects.get(username=username)
                template = loader.get_template('register.html')
            except User.DoesNotExist:
                email = request.POST['email']
                password = request.POST['password']
                newuser = User.objects.create_user(username, email, password)
                login(request, newuser)
                return redirect('/mispalabras')
        else:
            [template, context, red] = manage_comun_post(request)
            if red != "":
                return red
            else:
                template = loader.get_template('register.html')
    return HttpResponse(template.render(context, request))


def ayuda(request):
    [template, context, red] = manage_comun_post(request)
    if red != "":
        return red
    else:
        template = loader.get_template('ayuda.html')
        return HttpResponse(template.render(context, request))


def principal(request):
    palabra = ""
    response = process_file(request.GET.get('format', None), palabra)
    if response:
        return response
    [template, context, red] = manage_comun_post(request)
    if red != "":
        return red
    else:
        template = loader.get_template('inicio.html')
    pag = request.GET.get('pag', 1)
    [prevpag, act, nextpag] = get_scroll_nums(pag)
    context['lista_palabras'] = get_lista_palabras(int(pag))
    context['prev'] = prevpag
    context['next'] = nextpag
    context['num_list'] = act
    return HttpResponse(template.render(context, request))


def logout_view(request):
    logout(request)
    return redirect("/mispalabras/")


def buscarimagen(palabra):
    link = "https://es.wikipedia.org/w/api.php?action=query&titles=" + palabra + "&prop=pageimages&format=json&pithumbsize=200"
    try:
        objetojs = json.loads(str(urlopen(link).read().decode('utf-8')))
        texto_id = objetojs['query']['pages']
        num = str(texto_id).split("'")[1]
        urlimagen = texto_id[num]['thumbnail']['source']
    except:
        urlimagen = "http://apimeme.com/meme?meme=TED&top=Joe..&bottom=No+hay+imagen"
    return urlimagen


def buscartexto(palabra):
    try:
        link = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + palabra + "&prop=extracts&exintro&explaintext"
        document = parse(urlopen(link))
        definicion = document.find('query/pages/page/extract').text
        if not definicion:
            definicion = "Texto no encontrado en Wikipedia"
    except:
        definicion = "Texto no encontrado en Wikipedia"
    return definicion


def buscarRAE(palabra):
    url = "https://dle.rae.es/" + palabra
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf-8')
    soup = BeautifulSoup(html, 'html.parser')
    definicion = soup.find("meta", {"property": "og:description"})
    if not (definicion["content"].startswith(INICIO_RAE)):
        return "Texto no encontrado en RAE"
    return definicion["content"]


def buscarflickr(palabra):
    try:
        link = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + palabra
        scraper = cloudscraper.create_scraper()
        soup = BeautifulSoup(scraper.get(link).text, 'html.parser')
        urlimagen = soup.find('feed').findAll('entry')[0]
        for a in urlimagen.findAll('link', {'rel': 'enclosure'}, href=True):
            urlimagen = a['href']
        return urlimagen
    except:
        return "http://apimeme.com/meme?meme=TED&top=Joe..&bottom=No+hay+imagen"


def tarjeta_embebida(link):
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    try:
        req = urllib.request.Request(link, headers=headers)
        with urllib.request.urlopen(req) as response:
            html = response.read().decode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')
        descripcion = soup.find("meta", {"property": "og:description"})["content"]
        imagen = soup.find("meta", {"property": "og:image"})["content"]
        if descripcion is None:
            descripcion = soup.find("meta", {"property": "og:title"})["content"]
            if descripcion is None:
                descripcion = ""
        if imagen is None:
            imagen = "http://apimeme.com/meme?meme=Afraid-To-Ask-Andy&top=prueba& bottom=imagen+prueba"
        return descripcion, imagen
    except:
        return "", ""


def buscapalabra(request, palabra):
    ssl._create_default_https_context = ssl._create_unverified_context
    try:
        palabrabase = Palabras.objects.get(valor=palabra)
        template = loader.get_template('palabraexiste.html')
        imagenwiki = palabrabase.imagen_wiki
        textowiki = palabrabase.definicion
        response = process_file(request.GET.get('format', None), palabra)
        if response:
            return response

        try:
            voto = Votos.objects.get(usuario=request.user, palabra=palabrabase)
            # have_voted podria ser un booleano que indique true o false si está votado, pero siendo un entero podemos
            # saber el valor del voto para mostrar la otra opcion (si hay like muestro solo dislike)
            have_voted = voto.valor
        except:
            have_voted = 0
        can_vote = request.user.is_authenticated
        if request.method == "POST":
            if request.POST['name'] == "meme":
                opcion = request.POST['opcionmeme']
                if opcion == "1":
                    meme = "Afraid-To-Ask-Andy"
                elif opcion == "2":
                    meme = "Advice-Dog"
                elif opcion == "3":
                    meme = "Angry-Baby"
                else:
                    meme = "Blob"
                texto = request.POST['texto']
                urlmeme = "http://apimeme.com/meme?meme=" + meme + "&top=" + palabra + "&bottom=" + texto
                palabrabase.meme = urlmeme
                palabrabase.save()
            elif request.POST['name'] == "RAE":
                palabrabase.rae = buscarRAE(palabra)
                palabrabase.save()
            elif request.POST['name'] == "flickr":
                palabrabase.flickr = buscarflickr(palabra)
                palabrabase.save()
            elif request.POST['name'] == "formpalabra":
                valor = "/mispalabras/" + request.POST['palabra']
                return redirect(valor)
            elif request.POST['name'] == "login":
                principal(request)
            elif request.POST['name'] == "imagenpersonal":
                valor = request.FILES['valor']
                c = Imagenes(palabra=palabrabase, valor=valor, usuario=request.user, fecha=datetime.datetime.now())
                c.save()
            elif request.POST['name'] == "comentario":
                valor = request.POST['valor']
                c = Comentarios(palabra=palabrabase, valor=valor, usuario=request.user, fecha=datetime.datetime.now())
                c.save()
            elif request.POST['name'] == "enlace":
                valor = request.POST['valor']
                [descripcion, imagen] = tarjeta_embebida(valor)
                c = Enlaces(palabra=palabrabase, valor=valor, usuario=request.user, fecha=datetime.datetime.now(),
                            descripcion=descripcion, imagen=imagen)
                c.save()
            elif request.POST['name'] == "like":
                if have_voted == -1:
                    voto.valor = 1
                    voto.save()
                else:
                    c = Votos(palabra=palabrabase, valor=1, usuario=request.user, fecha=datetime.datetime.now())
                    c.save()
                have_voted = 1
            elif request.POST['name'] == "dislike":
                if have_voted == 1:
                    voto.valor = -1
                    voto.save()
                else:
                    c = Votos(palabra=palabrabase, valor=-1, usuario=request.user, fecha=datetime.datetime.now())
                    c.save()
                have_voted = -1
            elif request.POST['name'] == "delete":
                voto.delete()
                have_voted = 0

        lista_comentarios = Comentarios.objects.filter(palabra=palabrabase)
        lista_enlaces = Enlaces.objects.filter(palabra=palabrabase)
        lista_imagenes = Imagenes.objects.filter(palabra=palabrabase)
        votosbase = Votos.objects.filter(palabra=palabrabase)
        nvotos = 0
        for voto in votosbase:
            nvotos = nvotos + voto.valor
        base = Palabras.objects.all().values_list('valor')
        try:
            palabra = str(base[1]).split("'")[1]
        except:
            palabra = "No hay"
        pag = request.GET.get('pag', 1)
        context = get_main_elements(request)
        context2 = {"palabraactual": palabrabase.valor,
                   "imagen_wiki": imagenwiki,
                   "texto_wiki": textowiki,
                   "rae": palabrabase.rae,
                   "imagen_flickr": palabrabase.flickr,
                   "imagen_meme": palabrabase.meme,
                   "have_RAE": palabrabase.rae != "None",
                   "have_flickr": palabrabase.flickr != "None",
                   "comentarios_list": lista_comentarios,
                   "lista_enlaces": lista_enlaces,
                   "lista_imagenes": lista_imagenes,
                   "nvotos": nvotos,
                   "have_voted": have_voted,
                   "can_vote": can_vote,
                   "listapalabras": get_lista_palabras(pag)}
        context.update(context2)
    except Palabras.DoesNotExist:
        imagenwiki = buscarimagen(palabra)
        textowiki = buscartexto(palabra)
        template = loader.get_template('palabranoexiste.html')
        context = get_main_elements(request)
        context2 = {"palabraactual": palabra,
                    "imagen_wiki": imagenwiki,
                    "texto_wiki": textowiki
                    }
        context.update(context2)
        if request.method == "POST":
            if request.POST['name'] == "save":
                c = Palabras(valor=palabra, definicion=textowiki, imagen_wiki=imagenwiki, usuario=request.user,
                            fecha=datetime.datetime.now())
                c.save()
                return redirect('/mispalabras/' + palabra)
            elif request.POST['name'] == "formpalabra":
                valor = "/mispalabras/" + request.POST['palabra']
                return redirect(valor)
            elif request.POST['name'] == "login":
                principal(request)
    return HttpResponse(template.render(context, request))


def get_main_elements(request):
    context = {"lista_masvotado": masvotado(),
               "npalabras": Palabras.objects.all().__len__(),
               "palabraejemplo": get_example_word(),
               "mipaginapath": ("/mispalabras/usuarios/" + str(request.user))}
    return context


def get_num_list():
    npalabras = Palabras.objects.all().__len__()
    if npalabras == 0:
        return 1
    else:
        n = npalabras // N_POR_PAGINA
        if (npalabras % N_POR_PAGINA) != 0:
            n = n + 1
        return n


def mipagina(request, user):
    template = loader.get_template('mipagina.html')
    palabrasbase = Palabras.objects.filter(usuario=request.user).values()
    comentariosbase = Comentarios.objects.filter(usuario=request.user).values()
    enlacesbase = Enlaces.objects.filter(usuario=request.user).values()
    imagenesbase = Imagenes.objects.filter(usuario=request.user).values()
    listatotal = list(chain(palabrasbase, comentariosbase, enlacesbase, imagenesbase))
    listatotal = (sorted(listatotal, key=lambda x: x['fecha'], reverse=True))
    context = {"listatotal": listatotal,
               "lista_masvotado": masvotado(),
               "npalabras": Palabras.objects.all().__len__(),
               "palabraejemplo": get_example_word(),
               "mipaginapath": ("/mispalabras/usuarios/" + str(request.user))
               }
    return HttpResponse(template.render(context, request))


def errase(request):
    usuario = request.user
    logout(request)
    usuario.delete()
    return redirect('/mispalabras')


def masvotado():
    palabrasbase = Palabras.objects.all()
    dic = {}
    lista_palabras = []
    for palabra in palabrasbase:
        votosbase = Votos.objects.filter(palabra=palabra)
        nvotos = 0
        for voto in votosbase:
            nvotos = nvotos + voto.valor
        dic[palabra.valor] = nvotos
    dict_sort = sorted(dic.items(), key=lambda x: x[1], reverse=True)[:MOSTRAR_N_MAS_VOTADO]
    for palabra in dict_sort:
        lista_palabras.append(palabra[0])
    return lista_palabras
