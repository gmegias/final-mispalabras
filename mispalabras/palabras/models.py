from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Palabras(models.Model):
    tipo = models.CharField(max_length=8, default="palabra")
    valor = models.CharField(max_length=64)
    definicion = models.TextField(default="None", max_length=256)
    imagen_wiki = models.URLField(default="None")
    rae = models.TextField(default="None", max_length=256)
    flickr = models.URLField(default="None")
    meme = models.URLField(default="http://apimeme.com/meme?meme=Afraid-To-Ask-Andy&top=defecto& bottom=imagen+defecto")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField('publicado')


class Imagenes(models.Model):
    valor = models.ImageField(upload_to='imagenes', null=True)
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField('publicado')


class Enlaces(models.Model):
    tipo = models.CharField(max_length=7, default="enlace")
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    descripcion = models.TextField(default="None", max_length=256)
    imagen = models.URLField(default="None")
    valor = models.URLField(default="")
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField('publicado')


class Comentarios(models.Model):
    tipo = models.CharField(max_length=11, default="comentario")
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    valor = models.TextField(default="None", max_length=256)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField('publicado')


class Votos(models.Model):
    palabra = models.ForeignKey(Palabras, on_delete=models.CASCADE)
    valor = models.IntegerField(default=0)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    fecha = models.DateTimeField('publicado')