from django.contrib import admin
from .models import Palabras, Comentarios, Enlaces, Votos, Imagenes

admin.site.register(Palabras)
admin.site.register(Comentarios)
admin.site.register(Enlaces)
admin.site.register(Votos)
admin.site.register(Imagenes)