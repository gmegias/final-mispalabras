# Entrega practica

##Datos

* Nombre: Guillermo Megias Areas
* Titulación: Doble Grado (Ingeniería Aeroespacial en Aeronavegación e Ingeniería de Telecomunicaciones)
* Cuenta: gmegias
* Despliege (url): https://www.youtube.com/watch?v=pFmuEFlTy-k
* Video básico (url): https://www.youtube.com/watch?v=iy5iaSFr0j4
* Video parte opcional (url): http://gmegias.pythonanywhere.com/mispalabras

## Cuenta Admin Site
* admin/admin

## Cuentas usuarios
* troy/troy

## Resumen parte obligatoria
El footer presenta palabras aleatorias. Meme solo hay uno por palabra, se va modificando y "machacando".
Para la ejecución el programa hace uso de Beautiful Soup, requests, cloudscraper y pillow.


## Lista partes opcionales:
* Favicon
* Visualización de la página de palabra en formato JSON y XML
* Visualización de las palabras en formato JSON y XML
* Visualización de los comentarios en formato JSON y XML
* Permitir a un usuario poder borrar su cuenta
* Permitir deshacer el voto
* Permitir el voto en negativo o dislike
* Inclusión de imagenes de fichero con los comentarios
* Previous y Next para la navegacion de la página inicial
